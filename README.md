## Język Markdown

Tworzymy paragrafy odzielając tekst spacjami.

Możemy zapisać tekst *kursywą*, **pogrubić** albo ~~skreślić~~.

Do naszej dyspozycji także jest cytowanie
>I must not fear. Fear is the mind-killer. Fear is the little death that brings obliteration. 
>>I will face my fear and I will permit it to pass over me and through me. 
>>>And when it has gone past... I will turn the inner eye to see its path. Where the fear has gone there will be nothing. 
>>>>Only I will remain.

Kolejną opcją jest tworzenie list.
1. 1. Listy
   2. Są
   3. Super
2. - jeden
   - dwa
   - trzy 

Zawsze możemy przywitać świat!
``` print("Hello World") ```

![picture/myszunia.png](picture/myszunia.png)
